#!/bin/bash

MDOULE_NAME={1}


if [[ -z ${MDOULE_NAME} ]]; then
  echo "ERROR: Missing package name to be installed."
  exit 1
fi

if python3 -c "import ${MDOULE_NAME}"; then
  echo "INFO: Module ${MDOULE_NAME} is installed."
else
  echo "INFO: Module ${MDOULE_NAME} not found."
fi

exit 0
