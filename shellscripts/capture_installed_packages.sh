#!/bin/bash


INSTALLED_PACKAGES_LIST="./current_installed_package.list"

# capture installed packages
rpm -qa > ${INSTALLED_PACKAGES_LIST}

if [[ ${?} -eq 0 ]]; then
  echo "List of installed packages are stored in ${INSTALLED_PACKAGES_LIST} file."
else
  echo "Failed to capture list of installed packages, rpm command execution failed."
  exit 1
fi

exit 0

