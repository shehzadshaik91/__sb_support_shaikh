#!/bin/bash
#
#
# TESTING Commands
# touch messages-2021050{1,2,3,5,6,7,8,9}
# touch info-2021050{1,2,3,5,6,7,8,9}


# variable to store filenames and directory path
FILE_DIRECTORY="/root/charlieSB/delete_dir"
FILE_NAMES=(messages info)
LOGFILE="/tmp/delete_file.log"

echo -e "\n=========== Log clean-up run $(date) =============" >> ${LOGFILE}

# check if directory exist
echo "Validating if log directory is accessible and present" >> ${LOGFILE}
if [[ ! -d ${FILE_DIRECTORY} ]]; then
  echo "ERROR: directory ${FILE_DIRECTORY} not found or accessible."
fi

# delete all files that have date suffix
for file in ${FILE_NAMES[@]}
do
  # validate if info and messages log file is present with date suffix
  if [[ -f "${file}-*" ]]; then
    echo "INFO: Removing all of ${file} log files with date suffix." >> ${LOGFILE}
    find ${FILE_DIRECTORY} -maxdepth 1 -type f -name "${file}-*" -exec rm -f {} \;
  else
    echo "INFO: Log files with date suffix not found for ${file} log" >> ${LOGFILE}
  fi
done

exit 0
