#!/bin/bash

# Ran the command to install all packages, how do we know all of them are successfully installed?
# if you installed jq from maven, how do we know jq command is working as expected
# same for maven, ruby, python modules like jsonschema, pip, requests


# ---------- FUNCTINON ----------------------
# NAME: package_exist
# DESCRIPTION: 
# --------------------------------------------
package_exist(){
    package_name=${1}
    [[ $(rpm -q ${package_name} > /dev/null 2>&1) ]] && echo "true" || echo "false"
}

# ---------- FUNCTINON ----------------------
# NAME: package_exist
# DESCRIPTION: 
# --------------------------------------------
install_package(){
  package_name=${1}

  echo -e "\n======================= PACKAGE ${package_name} ======================="

  if [[ $(package_exist ${package_name}) == 'true' ]]; then
    echo "INFO: Provided package ${PACKAGE_NAME} is already installed."
  else
    yum install -y --skip-broken ${package_name} > ${LOG_FILE} 2>&1
    if [[ $(package_exist ${package_name}) == 'true' ]]; then
      echo "INFO: Provided package ${PACKAGE_NAME} installed successfully."
    else
      echo "ERROR: Failed to install provided package ${PACKAGE_NAME}."
      echo "Please refer to log file ${LOG_FILE} for troubleshooting."
      exit 1
    fi
  fi

}

# =================================================
# ==================== MAIN =======================
# =================================================
REQUIRED_PACKAGES="./packages.txt"
CURRENT_PACKAGES="/tmp/current_packages.txt"
INSTALLABLE_MISSING_PACKAGES="/tmp/installable_missing_packages.txt"
MISSING_REQUIRED_PACKAGES="/tmp/missing_required_packages.txt"
LOG_FILE="/tmp/validate_packages.log"

# removing staging files
trap '\
  rm -f \
    ${CURRENT_PACKAGES_LIST} \
    ${INSTALLABLE_MISSING_PACKAGES} \
    ${MISSING_REQUIRED_PACKAGES_LIST}' EXIT


if [[ ! -f ${REQUIRED_PACKAGES} ]]; then
  echo "ERROR: Missing packages list file."
  exit 1
fi

# Capture missing required packages list before installation
rpm -qa > ${CURRENT_PACKAGES}

awk 'NR==FNR{a[$0];next}!($0 in a)' ${CURRENT_PACKAGES} ${REQUIRED_PACKAGES} > ${INSTALLABLE_MISSING_PACKAGES}

# install missing require packages
cat ${INSTALLABLE_MISSING_PACKAGES} | xargs yum install -y --skip-broken > ${LOG_FILE} 2>&1

# Capture missing required packages list after installation
rpm -qa > ${CURRENT_PACKAGES}

awk 'NR==FNR{a[$0];next}!($0 in a)' ${CURRENT_PACKAGES} ${REQUIRED_PACKAGES} > ${MISSING_REQUIRED_PACKAGES}


if [[ $(wc -l < ${MISSING_REQUIRED_PACKAGES}) -ne 0 ]]; then
  echo -e "WARN: Faild to install follwing packages, refer to log file ${LOG_FILE} for more details\n"
  cat ${MISSING_REQUIRED_PACKAGES}
  exit 1
else
  echo "INFO: Below are the list of missing required packages that have been installed."
  cat ${INSTALLABLE_MISSING_PACKAGES}
fi

echo -e "\nINFO: All required packages are installed successfully."

exit 0
