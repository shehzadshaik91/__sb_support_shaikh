#!/bin/bash

REQUIRED_PACKAGES="/tmp/packages.txt"
CURRENT_PACKAGES="/tmp/current_packages.txt"
MISSING_PACKAGES="/tmp/missing_required_packages.txt"
INSTALLED_PACKAGES="/tmp/present_required_packages.txt"
# LOG_FILE="/tmp/validate_packages.log"

# removing staging files
trap 'rm -f ${CURRENT_PACKAGES}' EXIT


if [[ ! -f ${REQUIRED_PACKAGES} ]]; then
  echo "ERROR: Missing packages list file."
  exit 1
fi

# Capture missing required packages list before installation
rpm -qa > ${CURRENT_PACKAGES}

# capture missing packages from the server
awk 'NR==FNR{a[$0];next}!($0 in a)' ${CURRENT_PACKAGES} ${REQUIRED_PACKAGES} > ${MISSING_PACKAGES}

# capture installed packages that are present on server
awk 'NR==FNR{a[$0];next}!($0 in a)' ${MISSING_PACKAGES} ${REQUIRED_PACKAGES} > ${INSTALLED_PACKAGES}

echo -e "\nInstalled and Missing packages file list are stored as follows,"
echo "Installed Packages List....: ${INSTALLED_PACKAGES}"
echo "Missing Package List.......: ${MISSING_PACKAGES}"

exit 0

